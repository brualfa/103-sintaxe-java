package tech.mastertech.itau.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
//import java.util.TreeMap;
import java.util.TreeSet;

public class Collections {
	public static void main(String[] args) {
		List<String> lista = new ArrayList<>();
		
		lista.add("Banana");
		lista.add("Maçã");
		lista.add("Rabanete");
		
		for(String item : lista) {
			System.out.println(item);	
		}
		
		System.out.println(lista.size());
		
		lista.remove("Rabanete");
		System.out.println(lista.size());
		
		
		Set<String> conjunto = new TreeSet<>();
	
		conjunto.add("Rabanete");
		conjunto.add("Banana");
		conjunto.add("Maçã");
		conjunto.add("Banana");
		conjunto.add("Banana");
		
		for(String item : conjunto) {
			System.out.println(item);	
		}

		Set<OutraClasse> objetos = new HashSet<>();
		objetos.add(new OutraClasse(10));
		objetos.add(new OutraClasse(20));
		objetos.add(new OutraClasse(10));
		
		System.out.println(objetos.size());
		
		
		Map<String, OutraClasse> mapa = new HashMap<>();
//		Map<String, OutraClasse> mapa = new TreeMap<>();
		
		mapa.put("primeiro", new OutraClasse(2));
		mapa.put("segundo", new OutraClasse(544));
		mapa.put("terceiro", new OutraClasse(22222));
		mapa.put("segundo", new OutraClasse(55544));
		
		System.out.println(mapa);
		
		for(String chave : mapa.keySet()) {
			System.out.println("A chave "+ chave + " tem o valor: "+ mapa.get(chave));
		}
		
	}
}


class OutraClasse{
	private int qualquerCoisa;
	OutraClasse(int qualquerCoisa){
		this.qualquerCoisa = qualquerCoisa;
	}
	
	public String toString() {
		return ""+qualquerCoisa;
	}
}

