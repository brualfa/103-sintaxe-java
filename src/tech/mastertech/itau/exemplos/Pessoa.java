package tech.mastertech.itau.exemplos;

public class Pessoa {
	private int idade;
	private String nome;
	
	// Overloading!
	public Pessoa(String nome, int idade){
		this.nome = nome;
		this.idade = idade;
	}
	
	public Pessoa(int idade) {
		this.nome = "Não identificado";
		this.idade = idade;
	}
	
	public Pessoa() {
		this.nome = "Não identificado";
		this.idade = 0;
	}
	
	public String toString() {
		return "Meu nome é " + nome + " e tenho " + idade + " anos.";
	}
	
}
