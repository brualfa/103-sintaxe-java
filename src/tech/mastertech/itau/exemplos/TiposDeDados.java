package tech.mastertech.itau.exemplos;

public class TiposDeDados {
	int numero = 9;
	double decimal = 0.14;
	boolean creditoOuDebito = true;
	char letra = 'a';
	
	String frase = "Minha terra tem Corinthians onde canta o Tico Tico";
	
	int[] notas = {2, 5, 10, 20, 50, 100};
	int[] aindaNaoSei = new int[10];
}
