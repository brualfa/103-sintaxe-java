package tech.mastertech.itau.heranca;

public class App {
	public static void main(String[] args) {
		Professora professora = new Professora("Francisca", "Literatura");
		
		System.out.println(professora);
		
		System.out.println(professora instanceof Pessoa);
		System.out.println(professora instanceof Object);
		
		professora.dizerOi();
		
		professora.ensinar("JAVA");
		
	}
}
