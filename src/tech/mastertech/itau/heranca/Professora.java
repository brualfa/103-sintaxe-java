package tech.mastertech.itau.heranca;

public class Professora extends Pessoa implements ServicosDeSecretaria{
	private String profissao = "Professora";
	private String especialidade;
	
	public Professora(String nome, String especialidade) {
		super(nome);
		this.especialidade = especialidade;
	}
	
	public String getProfissao() {
		return profissao;
	}
	
	@Override
	public void dizerOi() {
		System.out.println("Oi aluninhos queridos do meu coração");
	}
	
	public void ensinar(String conteudo) {
		System.out.println("AE molecada! Hoje a aula é sobre "+conteudo);
	}

	
	// Métodos abstratos que precisam ser implementados
	// seja da classe abstrata ou da interface.
	@Override
	public void teste() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean preencherDiarios(String diario) {
		// TODO Auto-generated method stub
		return false;
	}
}
