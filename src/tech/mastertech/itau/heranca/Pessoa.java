package tech.mastertech.itau.heranca;

public abstract class Pessoa {
	public abstract void teste();
	
	private String nome;
	private int idade;
	private String documento;
	public Pessoa(String nome) {
		this.nome = nome;
	}
	
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	
	
	public void dizerOi() {
		System.out.println("Oi");
	}
	
}
